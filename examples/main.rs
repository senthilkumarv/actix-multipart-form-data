use actix_web::{App, HttpResponse, HttpServer, middleware, web};

use actix_multipart_form_data::MultipartForm;

pub async fn create(form: MultipartForm) -> Result<String, actix_web::Error> {
  println!("{:?}", form);
  Ok(String::new())
}

const INDEX_HTML: &str = r#"
<!DOCTYPE html>
<html>
<head>
    <meta charset=UTF-8>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Image Uploader</title>
</head>
<body>
<form method="post" action="/create" enctype="multipart/form-data">
    <input name="image" type="file" accept="image/*"/>
    <input type="text" name="first" />
    <input type="text" name="last" />
    <button type="submit">Upload The Image File</button>
</form>
</body>"#;

async fn index() -> HttpResponse {
  HttpResponse::Ok()
      .content_type("text/html")
      .body(format!("{}", INDEX_HTML))
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
  std::env::set_var("RUST_LOG", "actix_web=info");
  HttpServer::new(|| {
    App::new()
        .wrap(middleware::Logger::default())
        .service(web::resource("/index").to(index))
        .service(web::resource("/create").route(web::post().to(create)))
  })
      .bind("127.0.0.1:8080")?
      .start()
      .await
}
