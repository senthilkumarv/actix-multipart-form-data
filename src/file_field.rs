use std::fmt::{Debug, Error, Formatter};
use std::path::PathBuf;

use crate::multipart_form_items::{FormDataItem, MultipartExtractionError};

#[derive(Clone)]
pub struct FileField {
  file_name: String,
  content: Vec<u8>,
}

impl FileField {
  pub fn new(form_data_item: &FormDataItem) -> Result<FileField, MultipartExtractionError> {
    match form_data_item {
      FormDataItem::DataItem(_) => Err(MultipartExtractionError::InvalidType),
      FormDataItem::FileItem(file_name, content) => Ok(FileField {
        file_name: file_name.clone(),
        content: content.clone(),
      })
    }
  }

  pub fn file_name(&self) -> &str {
    self.file_name.as_str()
  }

  pub fn content(&self) -> &[u8] {
    self.content.as_slice()
  }

  pub fn save_to(&self, path_to_save: &str) -> Result<PathBuf, std::io::Error> {
    let file_path = PathBuf::from(format!("{}/{}", path_to_save, self.file_name));
    std::fs::write(&file_path, self.content.clone()).map(|_| file_path)
  }
}

impl Debug for FileField {
  fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
    write!(f, "Name: {} Size: {}", self.file_name, self.content.len())
  }
}