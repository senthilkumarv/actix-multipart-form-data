use crate::multipart_form_items::FormDataItem;

#[derive(Debug)]
pub struct MultipartEntry {
  pub name: String,
  pub value: FormDataItem,
}

impl MultipartEntry {
  pub fn create_text_field(field_name: &str, field_value: Vec<u8>) -> MultipartEntry {
    MultipartEntry {
      name: field_name.to_string(),
      value: FormDataItem::DataItem(field_value),
    }
  }

  pub fn create_file_field(field_name: &str, file_name: &str, field_value: Vec<u8>) -> MultipartEntry {
    MultipartEntry {
      name: field_name.to_string(),
      value: FormDataItem::FileItem(file_name.to_string(), field_value),
    }
  }
}
