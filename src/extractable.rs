use std::convert::Infallible;
use std::num::{ParseFloatError, ParseIntError};
use std::str::{ParseBoolError};

use chrono::{NaiveDateTime, Utc, DateTime};
use std::error::Error;
use std::fmt::Debug;

pub trait Extractable: Sized {
  type Error: Error + Debug + Sized;

  fn extract(val: &str) -> Result<Self, Self::Error>;
}

impl <T: Extractable> Extractable for Vec<T> {
  type Error = T::Error;

  fn extract(val: &str) -> Result<Self, Self::Error> {
    let a: Result<Vec<T>, Self::Error> = val.split(",")
      .map(|item| T::extract(item))
      .collect();
    a
  }
}

impl Extractable for String {
  type Error = Infallible;

  fn extract(val: &str) -> Result<Self, Self::Error> {
    Ok(val.to_string())
  }
}

impl Extractable for i64 {
  type Error = ParseIntError;

  fn extract(val: &str) -> Result<Self, Self::Error> {
    str::parse::<i64>(val)
  }
}

impl Extractable for i32 {
  type Error = ParseIntError;

  fn extract(val: &str) -> Result<Self, Self::Error> {
    str::parse::<i32>(val)
  }
}

impl Extractable for bool {
  type Error = ParseBoolError;

  fn extract(val: &str) -> Result<Self, Self::Error> {
    str::parse::<bool>(val)
  }
}

impl Extractable for f64 {
  type Error = ParseFloatError;

  fn extract(val: &str) -> Result<Self, Self::Error> {
    str::parse::<f64>(val)
  }
}

impl Extractable for f32 {
  type Error = ParseFloatError;

  fn extract(val: &str) -> Result<Self, Self::Error> {
    str::parse::<f32>(val)
  }
}

impl Extractable for NaiveDateTime {
  type Error = chrono::ParseError;

  fn extract(val: &str) -> Result<Self, Self::Error> {
    val.parse::<DateTime<Utc>>()
      .map(|date_time| date_time.naive_utc())
  }
}

#[test]
fn test_extract_i32_success() {
  let i: Result<i32, ParseIntError> = i32::extract("45");
  assert_eq!(i.is_ok(), true);
  assert_eq!(i.unwrap(), 45);
}

#[test]
fn test_extract_vector_i32_success() {
  let v: Result<Vec<i32>, ParseIntError> = Vec::extract("1,2,3");
  assert_eq!(v.is_ok(), true);
  assert_eq!(v.unwrap(), vec![1, 2, 3])
}

#[test]
fn test_extract_vector_string_success() {
  let v: Result<Vec<String>, Infallible> = Vec::extract("first str,2,3");
  assert_eq!(v.is_ok(), true);
  assert_eq!(v.unwrap(), vec!["first str", "2", "3"])
}

#[test]
fn test_extract_vector_float_success() {
  let v: Result<Vec<f32>, ParseFloatError> = Vec::extract("1,2,3");
  assert_eq!(v.is_ok(), true);
  assert_eq!(v.unwrap(), vec![1.0, 2.0, 3.0])
}

#[test]
fn test_extract_vector_i32_fail() {
  let v: Result<Vec<i32>, ParseIntError> = Vec::extract("1,j,3");
  assert_eq!(v.is_ok(), false);
}