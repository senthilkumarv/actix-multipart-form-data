use std::fmt::Formatter;
use std::error::Error;
use actix_http::ResponseError;
use actix_multipart::MultipartError;

#[derive(Debug)]
pub enum FormDataItem {
  DataItem(Vec<u8>),
  FileItem(String, Vec<u8>),
}

#[derive(Debug)]
pub enum MultipartExtractionError {
  FieldNotPresent,
  InvalidType,
  BadRequest,
}

impl std::error::Error for MultipartExtractionError {
  fn description(&self) -> &str {
    match self {
      MultipartExtractionError::FieldNotPresent => "Field you are looking for is not present",
      MultipartExtractionError::InvalidType => "Field you are looking for is present but does not match the datatype",
      MultipartExtractionError::BadRequest => "Request is not a valid multipart request"
    }
  }
}

impl std::fmt::Display for MultipartExtractionError {
  fn fmt(&self, fmt: &mut Formatter) -> Result<(), std::fmt::Error> {
    write!(fmt, ": {}", self.description())?;
    Ok(())
  }
}

impl ResponseError for MultipartExtractionError {
}

impl From<MultipartError> for MultipartExtractionError {
  fn from(_: MultipartError) -> Self {
    MultipartExtractionError::BadRequest
  }
}