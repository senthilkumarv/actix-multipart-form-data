use std::collections::HashMap;
use std::error::Error;
use std::pin::Pin;

use actix_http::Payload;
use actix_multipart::Multipart;
use actix_web::{FromRequest, HttpRequest};
use futures::{Future, FutureExt, StreamExt};

use crate::extractable::Extractable;
use crate::file_field::FileField;
use crate::multipart_entry::MultipartEntry;
use crate::multipart_form_items::{FormDataItem, MultipartExtractionError};

#[derive(Debug, Default)]
pub struct MultipartForm {
  items: HashMap<String, MultipartEntry>,
}

impl MultipartForm {
  pub(crate) fn new() -> MultipartForm {
    MultipartForm {
      items: HashMap::new()
    }
  }

  fn add(&mut self, entry: MultipartEntry) {
    let v = &mut self.items;
    v.insert(entry.name.clone(), entry);
  }

  pub fn extract<S: Extractable>(&self, field_name: &str) -> Result<S, MultipartExtractionError>
    where
        S: Extractable + Sized {
    self.extract_custom(field_name, |val| {
      S::extract(&String::from_utf8_lossy(val))
          .map_err(|_err| MultipartExtractionError::InvalidType)
    })
  }

  pub fn extract_custom<S, F, E: Error + Sized>(&self, field_name: &str, mapper: F) -> Result<S, MultipartExtractionError>
    where F: FnOnce(&Vec<u8>) -> Result<S, E> {
    match self.items.get(field_name) {
      Some(item) => {
        if let FormDataItem::DataItem(val) = &item.value {
          mapper(val)
              .map_err(|_err| MultipartExtractionError::InvalidType)
        } else {
          Err(MultipartExtractionError::InvalidType)
        }
      }
      None => Err(MultipartExtractionError::FieldNotPresent)
    }
  }

  pub fn extract_file(&self, field_name: &str) -> Result<FileField, MultipartExtractionError> {
    match self.items.get(field_name) {
      Some(item) => {
        if let FormDataItem::DataItem(_val) = &item.value {
          Err(MultipartExtractionError::InvalidType)
        } else {
          FileField::new(&item.value)
        }
      }
      None => Err(MultipartExtractionError::FieldNotPresent)
    }
  }

  async fn build_from_request(mut multipart: Multipart) -> Result<MultipartForm, MultipartExtractionError> {
    let mut form = MultipartForm::new();
    while let Some(Ok(mut field)) = multipart.next().await {
      if let Some(content_disposition) = field.content_disposition() {
        let mut data: Vec<u8> = Vec::new();
        while let Some(Ok(bytes)) = field.next().await {
          data.append(bytes.to_vec().as_mut())
        }
        let entry = match (content_disposition.get_name(), content_disposition.get_filename()) {
          (Some(field_name), Some(file_name)) => Some(MultipartEntry::create_file_field(field_name, file_name, data.to_vec())),
          (Some(field_name), None) => Some(MultipartEntry::create_text_field(field_name, data.to_vec())),
          _ => None
        };
        if let Some(e) = entry {
          form.add(e)
        }
      }
    }
    Ok(form)
  }
}

impl FromRequest for MultipartForm {
  type Error = MultipartExtractionError;
  type Future = Pin<Box<dyn Future<Output=Result<MultipartForm, MultipartExtractionError>>>>;
  type Config = ();

  fn from_request(req: &HttpRequest, payload: &mut Payload) -> Self::Future {
    MultipartForm::build_from_request(Multipart::new(req.headers(), payload.take()))
        .boxed_local()
  }
}

#[test]
fn should_extract_field_with_custom_extractor() {
  let entry_1 = MultipartEntry::create_text_field("field_1", "5".to_string().into_bytes());
  let entry_2 = MultipartEntry::create_text_field("field_2", "name".to_string().into_bytes());

  let mut form = MultipartForm::new();
  form.add(entry_1);
  form.add(entry_2);

  let result = form.extract_custom("field_1", |val| {
    Ok::<_, core::convert::Infallible>(format!("Received {}", String::from_utf8_lossy(val)))
  });

  assert_eq!(true, result.is_ok());
  assert_eq!("Received 5", result.unwrap());
}